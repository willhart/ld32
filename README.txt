# Colour Rocket
## A Ludum Dare 32 Entry, "An unconventional weapon"


# ABOUT

In the distant future the Empire of Humankind is ruled by evil black and white overlords who have leached all colour from the universe. A small band of rebels have been building a secret weapon which we believe will restore balance to the Empire.

Using your remote control system (WASD) pilot the weapon past the obstacles and defences until it reaches the Empire controlled planets and liberates them.


# NOTE

This is my first LD and my first attempt at making music. Thanks for playing and for your feedback... it's been a fun ride!

# CHANGE LOG

## Version 1.1

 - Tweaks to visuals
 - Tweaks to level difficulty
 - Add rocket flame lamp
 - Tweak camera victory animation

## Version 1.0

 - Initial version

# LICENSE

The MIT License (MIT)

Copyright (c) 2015, William Hart

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.