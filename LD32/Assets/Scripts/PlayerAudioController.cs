﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PlayerAudioController : MonoBehaviour {

    public AudioClip alertSound;
    public AudioClip hitSound;
    public AudioClip explodeSound;
    public AudioClip colourRocketSound;

    private AudioSource effectSource;

    void Awake()
    {
        this.effectSource = this.GetComponent<AudioSource>();
    }

    internal void PlayFailure()
    {
        this.effectSource.PlayOneShot(this.explodeSound);
    }

    internal void PlaySuccess()
    {
        this.StartCoroutine("DoPlaySuccess");
    }

    private IEnumerator DoPlaySuccess()
    {
        this.effectSource.PlayOneShot(this.hitSound);
        yield return new WaitForSeconds(0.2f);
        this.effectSource.PlayOneShot(this.explodeSound);
        yield return new WaitForSeconds(0.2f);
        this.effectSource.PlayOneShot(this.colourRocketSound);
    }
}
