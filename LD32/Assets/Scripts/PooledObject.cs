﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class PooledObject : MonoBehaviour 
{

    public event EventHandler OnSleep;

    public float MinXVelocity = -100f;
    public float MaxXVelocity = -10f;
    public float MaxLateralVelocity = 2f;
    public bool usePreconfiguredPosition = false;

    private Rigidbody rb;
    private float originalXVelocity;
    private float originalYVelocity;
    private float originalZVelocity;

    private Material mainMaterial;
    private float currentGreyscale = 1f;
    private float greyscaleDamping = 0.5f;
    private bool isDead = false;

    void Awake()
    {
        this.rb = this.GetComponent<Rigidbody>();
        this.mainMaterial = this.GetComponentInChildren<Renderer>().material;
    }

    void OnEnable() 
    {
        this.isDead = false;

        if (!this.usePreconfiguredPosition)
        {
            var x = UnityEngine.Random.Range(TargetEntity.CurrentXValue, TargetEntity.CurrentXValue + 50f);
            var y = UnityEngine.Random.Range(-10, 10);
            var z = UnityEngine.Random.Range(-30, 30);
            this.transform.position = new Vector3(x, y, z);
        }

        this.originalXVelocity = UnityEngine.Random.Range(this.MinXVelocity, this.MaxXVelocity);
        this.originalYVelocity = UnityEngine.Random.Range(-this.MaxLateralVelocity, this.MaxLateralVelocity);
        this.originalZVelocity = UnityEngine.Random.Range(-this.MaxLateralVelocity, this.MaxLateralVelocity);

        this.rb.velocity = new Vector3(this.originalXVelocity, this.originalYVelocity, this.originalZVelocity);
        this.rb.angularVelocity = new Vector3(UnityEngine.Random.Range(0, 3f), UnityEngine.Random.Range(0, 3f), UnityEngine.Random.Range(0, 3f));

        var randomScale = UnityEngine.Random.Range(0.5f, 2f);
        this.transform.localScale = new Vector3(randomScale, randomScale, randomScale);

        this.StartCoroutine("ScaleObjectIn", 1f);
    }
    
    void FixedUpdate()
    {
        this.rb.velocity = new Vector3(this.originalXVelocity,
            Mathf.Lerp(this.rb.velocity.y, this.originalYVelocity + PlayerController.YMovement, Time.deltaTime * 5f),
            Mathf.Lerp(this.rb.velocity.z, this.originalZVelocity + PlayerController.ZMovement, Time.deltaTime * 5f));
    }

    void Update()
    {
        if (!isDead)
        {
            return;
        }

        this.mainMaterial.SetFloat("_EffectAmount", this.currentGreyscale);
        this.currentGreyscale = Mathf.Lerp(this.currentGreyscale, 0, Time.deltaTime * this.greyscaleDamping);
    }

    /// <summary>
    /// Scales the pooled object in to reduce "pop in"
    /// </summary>
    /// <param name="overTime"></param>
    /// <returns></returns>
    private IEnumerator ScaleObjectIn(float overTime)
    {
        var delta = overTime / 100;
        var startingScale = this.transform.localScale.x;
        var scaleDelta = startingScale / 100f;
        var currentScale = 0f;

        this.transform.localScale = Vector3.zero;
        
        for (var i = 0; i < 100; ++i)
        {
            yield return new WaitForSeconds(delta);
            currentScale += scaleDelta;

            this.transform.localScale = new Vector3(currentScale, currentScale, currentScale);
        }
    }

    /// <summary>
    /// Sends the object back to sleep in the object pools
    /// </summary>
    public void GoToSleep()
    {
        this.gameObject.SetActive(false);

        var os = this.OnSleep;
        if (os != null)
        {
            os(this, new EventArgs());
        }
    }

    public void Die()
    {
        this.isDead = true;
    }
}
