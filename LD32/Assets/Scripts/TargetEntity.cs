﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class TargetEntity : MonoBehaviour {

    public static float CurrentXValue;

    public float originalXVelocity;

    private Rigidbody rb;

    public float greyscaleDamping = 7f;
    private bool isDead = false;
    private float currentGreyscale = 1;
    private List<Material> allMats;

    void Awake()
    {
        CurrentXValue = this.transform.position.x;
        rb = this.GetComponent<Rigidbody>();
        rb.velocity = new Vector3(this.originalXVelocity, 0, 0);

        this.allMats = new List<Material>();
        var renderers = this.GetComponentsInChildren<Renderer>();
        foreach (var r in renderers)
        {
            this.allMats.AddRange(r.materials);
        }

        if (PlayerDeath.Instance != null)
        {
            PlayerDeath.Instance.OnTargetHit += OnHitByMissile;
        }

        this.isDead = false;
    }

    void OnHitByMissile(object sender, System.EventArgs e)
    {
        this.Die();
    }

    void FixedUpdate()
    {
        if (!isDead)
        {
            this.rb.velocity = new Vector3(this.originalXVelocity, 
                Mathf.Lerp(this.rb.velocity.y, PlayerController.YMovement, Time.deltaTime * 5f), 
                Mathf.Lerp(this.rb.velocity.z, PlayerController.ZMovement, Time.deltaTime * 5f));
        }
        else
        {
            this.rb.velocity = new Vector3(this.originalXVelocity * 0.1f, 0, 0);
        }
    }

    void Update()
    {
        CurrentXValue = this.transform.position.x;

        if (!isDead)
        {
            return;
        }

        foreach (var mat in this.allMats)
        {
            mat.SetFloat("_EffectAmount", this.currentGreyscale);
        }
        this.currentGreyscale = Mathf.Lerp(this.currentGreyscale, 0, Time.deltaTime * this.greyscaleDamping);
    }

    public void Die()
    {
        this.isDead = true;
        this.rb.velocity = Vector3.zero;
    }
}
