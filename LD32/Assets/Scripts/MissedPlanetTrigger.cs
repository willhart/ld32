﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class MissedPlanetTrigger : MonoBehaviour {
    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<PlayerDeath>() != null)
        {
            PlayerDeath.Instance.MissedPlanet();
        }
    }
}
