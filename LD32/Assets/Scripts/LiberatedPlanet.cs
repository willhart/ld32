﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LiberatedPlanet : MonoBehaviour {
    public string levelName;
    public string planetName;
    public string planetDescription;

    public PlanetUILauncher planetUI;

    private List<Material> allMats;

    void Awake()
    {
        this.allMats = new List<Material>();
        var renderers = this.GetComponentsInChildren<Renderer>();

        foreach (var r in renderers)
        {
            this.allMats.AddRange(r.materials);
        }

        this.SetLiberated(false);
        CampaignStateManager.Instance.OnLevelStateChanged += OnLevelStateChanged;
    }

    void Start()
    {
        if (CampaignStateManager.Instance != null)
        {
            this.SetLiberated(CampaignStateManager.Levels[this.levelName]);
        }
    }

    private void OnLevelStateChanged(object sender, BooleanEventArgs e)
    {
        var level = sender as string;
        if (level == this.levelName)
        {
            this.SetLiberated(e.Value);
        }
    }

    public void SetLiberated(bool liberated)
    {
        foreach (var mat in this.allMats)
        {
            mat.SetFloat("_EffectAmount", liberated ? 0 : 1f);
        }
    }

    void OnMouseOver()
    {
        bool complete = false;
        if (CampaignStateManager.Instance != null)
        {
            complete = CampaignStateManager.Levels[this.levelName];
        }

        this.planetUI.SetupUI(this.transform.position + new Vector3(10f, 10f, 0), this.planetName, this.planetDescription, complete);
    }

    void OnMouseExit()
    {
        this.planetUI.HideUI();
    }

    void OnMouseUp()
    {
        Application.LoadLevel(this.levelName);
    }
}
