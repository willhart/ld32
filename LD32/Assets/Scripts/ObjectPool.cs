﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoBehaviour {

    public PooledObject objectToPool;
    public int numberToPool;
    public bool continuousSpawn = true;

    private List<GameObject> pooledObjects;
    private int lastIdx;

    private bool isDead = false;

    void Awake()
    {
        this.pooledObjects = new List<GameObject>();

        for (var i = 0; i < this.numberToPool; ++i)
        {
            var obj = Instantiate(this.objectToPool.gameObject) as GameObject;
            obj.SetActive(false);
            obj.GetComponent<PooledObject>().OnSleep += OnPooledObjectSleep;
            this.pooledObjects.Add(obj);
        }

        if (PlayerDeath.Instance != null)
        {
            PlayerDeath.Instance.OnTargetHit += OnTargetHit;
        }
    }

    void OnTargetHit(object sender, System.EventArgs e)
    {
        this.Die();
    }

    void OnPooledObjectSleep(object sender, System.EventArgs e)
    {
        if (this.continuousSpawn)
        {
            var po = sender as PooledObject;
            po.gameObject.SetActive(true);
        }
    }

    void Update()
    {
        if (!isDead)
        {
            return;
        }

        foreach (var obj in this.pooledObjects)
        {
            if (obj.activeInHierarchy)
            {
                obj.GetComponent<PooledObject>().Die();
            }
        }

        isDead = false;
    }

    /// <summary>
    /// Gets the next game object
    /// </summary>
    /// <returns></returns>
    public GameObject GetObject()
    {
        for (var i = this.lastIdx; i < this.numberToPool; ++i)
        {
            if (this.pooledObjects[i].activeInHierarchy)
            {
                continue;
            }

            this.lastIdx = i;
            return this.pooledObjects[i];
        }

        for (var i = 0; i < this.lastIdx; ++i)
        {
            if (this.pooledObjects[i].activeInHierarchy)
            {
                continue;
            }

            this.lastIdx = i;
            return this.pooledObjects[i];
        }

        return null;
    }

    public void Die()
    {
        this.isDead = true;
    }
}
