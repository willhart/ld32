﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class BooleanEventArgs : EventArgs
{
    public bool Value;
}

public class CampaignStateManager : MonoBehaviour
{
    public event EventHandler<BooleanEventArgs> OnLevelStateChanged;

    public static Dictionary<string, bool> Levels = new Dictionary<string, bool>() 
    {
        { "Level01", false },
        { "Level02", false },
        { "Level03", false }
    };

    private static CampaignStateManager _instance;

    void Awake()
    {
        if (_instance == null)
        {
            // I am the one
            _instance = this;
        }
        else
        {
            // there can only be one
            Destroy(this);
            return;
        }

        // persist this object
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        var tempDict = new Dictionary<string, Boolean>();

        // check we have the correct keys
        foreach (var key in Levels.Keys)
        {
            var olsc = this.OnLevelStateChanged;

            if (!PlayerPrefs.HasKey(key))
            {
                PlayerPrefs.SetInt(key, Levels[key] == false ? 0 : 1);

                if (olsc != null)
                {
                    olsc(key, new BooleanEventArgs() { Value = false });
                }
            }
            else
            {
                tempDict.Add(key, PlayerPrefs.GetInt(key) > 0);
                if (olsc != null)
                {
                    olsc(key, new BooleanEventArgs() { Value = tempDict[key] });
                }
            }
        }

        // merge in changes
        foreach (var t in tempDict.Keys)
        {
            Levels[t] = tempDict[t];
        }
    }

    internal void CompleteLevel(string p)
    {
        if (!Levels.ContainsKey(p))
        {
            Debug.LogWarning("Unable to complete level with unknown key - " + p);
            return;
        }

        PlayerPrefs.SetInt(p, 1);
        Levels[p] = true;
        
        var olsc = this.OnLevelStateChanged;
        if (olsc != null)
        {
            olsc(p, new BooleanEventArgs() { Value = true });
        }
    }

    internal void ResetCampaign()
    {
        var olsc = this.OnLevelStateChanged;
        var keys = new List<string>(Levels.Keys);
        foreach (var key in keys)
        {
            Levels[key] = false;
            PlayerPrefs.SetInt(key, 0);

            if (olsc != null)
            {
                olsc(key, new BooleanEventArgs() { Value = false });
            }
        }
    }

    public static CampaignStateManager Instance
    {
        get
        {
            return _instance;
        }
    }
}
