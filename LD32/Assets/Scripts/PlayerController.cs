﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerController : MonoBehaviour {

    // The amount of Z movement from a player
    public static float ZMovement = 0;
    public static float YMovement = 0;

    public float playerFuel = 100;

    public float fuelConsumptionRate = 0.015f;

    public Slider fuelSlider;

    public float lateralSpeed;

    public Renderer meshRenderer;

    private bool isDead = false;

    private bool isPaused = false;

    public void Awake()
    {
        if (this.fuelSlider != null)
        {
            this.fuelSlider.maxValue = this.playerFuel;
        }

        this.isDead = false;
        Time.timeScale = 1f;
    }

    void Update()
    {
        if (this.playerFuel <= 0 || this.isDead)
        {
            ZMovement = 0;
            YMovement = 0;
        }
        else
        {
            ZMovement = Input.GetAxis("Horizontal") * this.lateralSpeed;
            YMovement = -Input.GetAxis("Vertical") * this.lateralSpeed;
        }

        if (Input.GetButtonDown("Cancel"))
        {
            this.isPaused = !this.isPaused;

            if (this.isPaused)
            {
                Time.timeScale = 0;
                FindObjectOfType<GameMenuManager>().ShowVictoryMenu(null);
            }
            else 
            {
                Time.timeScale = 1;
                FindObjectOfType<GameMenuManager>().HideVictoryMenu();
            }
        }
    }

    void FixedUpdate()
    {
        this.playerFuel -= (Mathf.Abs(YMovement) * this.fuelConsumptionRate + Mathf.Abs(ZMovement) * this.fuelConsumptionRate);

        if (this.fuelSlider != null)
        {
            this.fuelSlider.value = this.playerFuel;
        }
    }

    public void Die()
    {
        this.isDead = true;
        this.StartCoroutine("DisableMissile");
    }

    private IEnumerator DisableMissile()
    {
        yield return new WaitForSeconds(2f);
        this.meshRenderer.enabled = false;
    }
}
