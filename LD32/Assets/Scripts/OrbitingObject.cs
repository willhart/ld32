﻿using UnityEngine;
using System.Collections;

public class OrbitingObject : MonoBehaviour {
    public Transform orbitCentre;

    void Update()
    {
        // http://answers.unity3d.com/questions/585175/how-can-i-make-a-gameobject-orbit-another.html
        var relativePos = this.orbitCentre.position + new Vector3(0, 1.5f, 0) - this.transform.position;
        var rotation = Quaternion.LookRotation(relativePos);
        var current = transform.localRotation;

        this.transform.localRotation = Quaternion.Slerp(current, rotation, Time.deltaTime);
        transform.Translate(0, 0, 3 * Time.deltaTime);
    }
}
