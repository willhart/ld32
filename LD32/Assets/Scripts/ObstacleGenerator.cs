﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ObjectPool))]
public class ObstacleGenerator : MonoBehaviour {

    public int obstaclesToUse = 10;

    private ObjectPool objectPool;

    void Awake()
    {
        this.objectPool = this.GetComponent<ObjectPool>();
    }

	void Start () {
        for (var i = 0; i < this.obstaclesToUse; ++i)
        {
            var obj = this.objectPool.GetObject();
            obj.SetActive(true);
        }
	}
}
