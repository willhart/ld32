﻿using UnityEngine;
using System;
using System.Collections;

public class DeathCam : MonoBehaviour {

    private Animator anim;

	// Use this for initialization
    void Awake()
    {
        this.anim = this.GetComponent<Animator>();

        PlayerDeath.Instance.OnTargetHit += OnTargetHit;
    }

    private void OnTargetHit(object sender, EventArgs e)
    {
        this.anim.SetTrigger("TargetHit");
    }
}
