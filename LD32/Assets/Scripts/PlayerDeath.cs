﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(PlayerAudioController))]
public class PlayerDeath : MonoBehaviour {

    public event EventHandler OnTargetHit;

    public GameObject deathParticleSystem;

    private static PlayerDeath _instance;
    private Vector3 deathParticleLocation;
    private bool isDead = false;
    private PlayerAudioController audioController;

    public void Awake()
    {
        this.isDead = false;
        this.audioController = this.GetComponent<PlayerAudioController>();
    }

    void OnTriggerEnter(Collider other)
    {
        var obstacle = other.GetComponent<PooledObject>();
        var placedObstacle = other.GetComponent<StandardObstacle>();

        if (obstacle != null || placedObstacle != null)
        {
            // we have collided with an obstacle, die
            this.audioController.PlayFailure();
            Time.timeScale = 0;
            this.isDead = true;
            FindObjectOfType<GameMenuManager>().ShowVictoryMenu(false);
            return;
        }

        var target = other.GetComponent<TargetEntity>();
        if (target != null)
        {
            this.isDead = true;

            // stop colliding and removing objects
            this.GetComponent<Collider>().enabled = false;
            this.GetComponent<PlayerController>().Die();
            this.StartCoroutine("ShowVictoryMenu");

            this.audioController.PlaySuccess();
            
            // play particle effect
            if (this.deathParticleSystem != null)
            {
                this.deathParticleLocation = this.deathParticleSystem.transform.position;
                this.deathParticleSystem.SetActive(true);
            }

            // raise the event to let other components handle the death
            var oth = this.OnTargetHit;
            if (oth != null)
            {
                oth(this, new EventArgs());
            }
        }
    }

    void Update()
    {
        if (!this.isDead)
        {
            return;
        }

        // park the death particle system
        if (this.deathParticleSystem != null)
        {
            this.deathParticleSystem.transform.position = this.deathParticleLocation;
        }
    }

    public void MissedPlanet()
    {
        Debug.Log("Missed!");
        FindObjectOfType<GameMenuManager>().ShowVictoryMenu(false);
        Time.timeScale = 0;
        this.isDead = true;
        
    }

    private IEnumerator ShowVictoryMenu()
    {
        yield return new WaitForSeconds(7f);
        FindObjectOfType<GameMenuManager>().ShowVictoryMenu(true);
    }

    public static PlayerDeath Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PlayerDeath>();
            }

            return _instance;
        }
    }
}
