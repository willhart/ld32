﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameMenuManager : MonoBehaviour {

    public GameObject victoryMenu;
    public Text victoryText;

    public void ReturnToMenu()
    {
        Application.LoadLevel("Campaign");
    }

    public void RestartLevel()
    {
        this.victoryMenu.SetActive(false);
        Application.LoadLevel(Application.loadedLevel);
    }

    public void ShowVictoryMenu(bool? success)
    {
        this.victoryText.text = success == null ? "GAME PAUSED" : (success == true ? "VICTORY!" : "DEFEAT!");
        this.victoryMenu.SetActive(true);
        CampaignStateManager.Instance.CompleteLevel(Application.loadedLevelName);
    }

    internal void HideVictoryMenu()
    {
        this.victoryMenu.SetActive(false);
    }
}
