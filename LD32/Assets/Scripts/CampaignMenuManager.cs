﻿using UnityEngine;
using System.Collections;

public class CampaignMenuManager : MonoBehaviour
{
    public void OnBackButtonClick()
    {
        Application.LoadLevel(0);
    }

    public void OnCampaignResetButtonClick()
    {
        CampaignStateManager.Instance.ResetCampaign();
    }
}
