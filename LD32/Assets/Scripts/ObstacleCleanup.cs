﻿using UnityEngine;
using System.Collections;

public class ObstacleCleanup : MonoBehaviour {
    void Awake()
    {
        if (PlayerDeath.Instance != null)
        {
            PlayerDeath.Instance.OnTargetHit += OnTargetHit;
        }
    }

    void OnTargetHit(object sender, System.EventArgs e)
    {
        this.gameObject.SetActive(false);
    }

    void OnTriggerEnter(Collider other)
    {
        var pooledObj = other.GetComponent<PooledObject>();

        if (pooledObj == null)
        {
            return;
        }

        pooledObj.GoToSleep();
    }
}
