﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

    public GameObject ExitButton;

    public void Awake()
    {
        if (Application.isWebPlayer || Application.isMobilePlatform)
        {
            this.ExitButton.SetActive(false);
        }

        Time.timeScale = 1f;
    }

    public void OnPlayButtonClick()
    {
        Application.LoadLevel("Campaign");
    }

    public void OnExitButtonClick()
    {
        Application.Quit();
    }
}
