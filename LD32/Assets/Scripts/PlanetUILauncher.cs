﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlanetUILauncher : MonoBehaviour {
    public Text planetName;
    public Text planetDescription;

    public void SetupUI(Vector3 location, string name, string description, bool liberated)
    {
        this.transform.position = location;
        this.planetName.text = name + (liberated ? " [liberated]" : "");
        this.planetDescription.text = description;
        this.gameObject.SetActive(true);
    }

    public void HideUI()
    {
        this.gameObject.SetActive(false);
    }
}
